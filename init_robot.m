function robot = init_robot(q, t_travel, id)

robot.q = q;
robot.t_travel = t_travel;
robot.id = id;
robot.traj = q;
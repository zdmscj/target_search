function [Env, Alpha, Beta] = calc_reward(Alpha, Beta, goal, action, Env)

Reward = zeros(1, length(Env.grids));
for i = 1 : length(Env.grids)
    Reward(i) = Env.grids{i}(4);
    if Reward(i) < 0
        keyboard
    end
end

for i = 1 : Env.width
    for j = 1 : Env.height
        idx = i + (j-1) * Env.height;
        Reward(idx) = 1 - norm(goal - Env.grids{idx}(1:2))/sqrt(Env.width^2 + Env.height^2);
    end    
end

if Env.grids{action}(3) 
    Alpha = Alpha + Reward;
    Beta = Beta + 1 - Reward;
else
    Alpha = Alpha + 1 - Reward;
    Beta = Beta + Reward;
end

% for i = 1 : Env.width
%     for j = 1 : Env.height
%         idx = i + (j-1) * Env.height;
%         Env.grids{idx}(4) = Alpha(idx);
%         Env.grids{idx}(5) = Beta(idx);
%     end    
% end

function data = run_simulation(params)

% Environment
Env = init_env(params); 

% Robot
q0 = [1,1];
t_travel = params.t_travel;
t_detect = params.t_detect;
robot = init_robot(q0, t_travel, 1);

% Initialize
t = 0;
sc = [];
goal = q0;
goal_set = q0;
if params.method
    Alpha0 = ones(1, Env.num_grid);
    Beta0 = ones(1, Env.num_grid);
    action0 = 1;
    action_set = 0;  
    get_reward = 0;
    no_reward = 0;
    [goal, action, Alpha, Beta, get_reward, no_reward] = ThompsonSampling(Env, ...
        goal, action0, action_set, Alpha0, Beta0, get_reward, no_reward, params); 
    goal_set = [q0; goal];
end
visited_aoi = [];
data.t = zeros(length(params.percentage) - 1, 1);

% Explore
while true
    robot = move_robot(robot, goal);
    if params.animation
        if params.method
            animation(Env, robot, goal_set, Alpha, Beta)
        else
            animation(Env, robot, goal_set)
        end
    end
    if norm(robot.q - goal) < 0.5  
        t = t + t_detect;
        if params.method
            [goal, action, Alpha, Beta, get_reward, no_reward] = ThompsonSampling(Env, ...
                goal, action, action_set, Alpha, Beta, get_reward, no_reward, params); 
            if params.edge_filtering
                Beta = edge_filtering(Alpha, Beta, Env);
            end            
            action_set = [action_set, action];
        else
            goal = sweeping(Env, goal);
        end
        goal_set = [goal_set; goal];
        idx = goal(1) + (goal(2) - 1) * Env.height;
        if idx > Env.num_grid
            data.sc = sc;
            break
        end
        if Env.grids{idx}(3)
            visited_aoi = [visited_aoi, idx];
        end
    else
        t = t + 1;
    end     
    sc = [sc; t, ws_distance(goal_set', Env.aoi_coord')];
    for i = 1 : length(params.percentage)
        if length(visited_aoi) == ceil(length(Env.aoi) * params.percentage(i))
            data.t(i) = t;
        end
    end
    if length(visited_aoi) >= floor(length(Env.aoi) * params.percentage(end))
        data.sc = sc;
        if params.method
            data.alpha = Alpha;
            data.beta = Beta;
        end
        break
    end 
end





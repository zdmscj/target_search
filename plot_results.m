function plot_results(data, m, e, x_lim, y_lim)

figure

if length(data) == 1
    plot(data{1}.sc(:,1), data{1}.sc(:,2));
else
    for i = 1 : length(data)
        plot(data{i}.sc(:,1), data{i}.sc(:,2));
        hold on
        
    end
    pl = length(data{i}.t);
    for j = 1 : pl
        switch j
            case 1
                c = 'r';
            case 2
                c = 'b';
            case 3
                c = 'y';
            otherwise
                error('Only support three colors')
        end
        plot([median(data{i}.t(j)), median(data{i}.t(j))], [0, y_lim], '-', 'color', c)
        hold on
    end

end
if m
    if e
        title('TS with ef: Wasserstein distance')
    else
        title('TS without ef: Wasserstein distance')
    end
else
    title('Sweeping: Wasserstein distance')
end
xlim([0, x_lim])
ylim([0, y_lim])
hold off

shg
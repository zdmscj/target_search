clear
clc
close all

% Environment
params.env_width = 20;
params.env_height = 20;

% Robot
params.t_travel = 1;
params.t_detect = 5;

% Random seed
c = clock;
rn = floor(c(5)*c(6));
params.seed = rn;

% Terminate condition
params.percentage = [0.5, 0.9, 1];  

% Methods
params.method = 1;         % 0: sweeping; 1: ts
params.edge_filtering = 1;

% TS settings
params.reward = 10;         % E/E trade-off

% Simulation settings
params.animation = 1;
single = 1;
x_lim = 5000;
y_lim = 20;
num_batch = 30;

%% Run single
if single
    data = run_simulation(params);
    plot_results({data}, 0, params.method, x_lim, y_lim)
else
%% Run batch
    data_batch = cell(num_batch);
    seeds = zeros(1, num_batch);
    for i = 1 : num_batch
        seeds(i) = rn + i;
    end
    for m = [0, 1]
        params.method = m;
        if m 
            for e = [0, 1]
                for i = 1 : num_batch
                    params.round = i;
                    params.seed = seeds(i);
                    params.edge_filtering = e;
                    data_batch{i} = run_simulation(params);
                    disp(['Round ', num2str(i), '; ts: ', num2str(m), '; edge: ', num2str(e)])
                end
                plot_results(data_batch, m, e, x_lim, y_lim)
            end     
        else
            for i = 1 : num_batch
                params.round = i;
                params.seed = seeds(i);
                data_batch{i} = run_simulation(params);
                disp(['Round ', num2str(i), '; ts: ', num2str(m),])
            end
            plot_results(data_batch, m, 0, x_lim, y_lim)
        end
    end
end



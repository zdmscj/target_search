function Env = init_env(params)

Env = struct;
Env.width = params.env_width;
Env.height = params.env_height;
Env.num_grid = Env.height * Env.height;
Env.grids = cell(1, Env.width * Env.height);
for i = 1 : Env.width
    for j = 1 : Env.height
        Env.grids{i + (j-1) * Env.height} = [i, j, false]; %[row, column, aoi] 
                                                           % upper-right corner of each grid                                                                                                                                            
    end
end

% Area of Interest
Env.aoi = [];
Env.aoi_coord = [];
rng(params.seed)

% random clusters
rni = randi(floor(4*Env.width/5));
rnj = randi(floor(5*Env.height/6));
for i = 1+rni : floor(Env.width/5)+rni
    for j = 1+rnj : floor(Env.height/6)+rnj
        Env.grids{i + (j-1) * Env.height}(3) = true;
        Env.aoi = [Env.aoi, i + (j-1) * Env.height];
        Env.aoi_coord = [Env.aoi_coord; i, j];
    end
end

rni = randi(floor(5*Env.width/6));
rnj = randi(floor(4*Env.height/5));
for i = 1+rni : floor(Env.width/6)+rni
    for j = 1+rnj : floor(Env.height/5)+rnj
        idx = i + (j-1) * Env.height;
        if ~ismember(idx, Env.aoi)
            Env.grids{idx}(3) = true;
            Env.aoi = [Env.aoi, idx];
            Env.aoi_coord = [Env.aoi_coord; i, j];
        end
    end
end

rni = randi(floor(4*Env.width/5));
rnj = randi(floor(3*Env.height/4));
for i = 1+rni : floor(Env.width/5)+rni
    for j = 1+rnj : floor(Env.height/4)+rnj
        idx = i + (j-1) * Env.height;
        if ~ismember(idx, Env.aoi)
            Env.grids{idx}(3) = true;
            Env.aoi = [Env.aoi, idx];
            Env.aoi_coord = [Env.aoi_coord; i, j];
        end
    end
end

rni = randi(floor(4*Env.width/5));
rnj = randi(floor(3*Env.height/4));
for i = 1+rni : floor(Env.width/5)+rni
    for j = 1+rnj : floor(Env.height/4)+rnj
        idx = i + (j-1) * Env.height;
        if ~ismember(idx, Env.aoi)
            Env.grids{idx}(3) = true;
            Env.aoi = [Env.aoi, idx];
            Env.aoi_coord = [Env.aoi_coord; i, j];
        end
    end
end

rni = randi(floor(6*Env.width/7));
rnj = randi(floor(7*Env.height/8));
for i = 1+rni : floor(Env.width/7)+rni
    for j = 1+rnj : floor(Env.height/8)+rnj
        idx = i + (j-1) * Env.height;
        if ~ismember(idx, Env.aoi)
            Env.grids{idx}(3) = true;
            Env.aoi = [Env.aoi, idx];
            Env.aoi_coord = [Env.aoi_coord; i, j];
        end
    end
end

rni = randi(floor(7*Env.width/8));
rnj = randi(floor(6*Env.height/7));
for i = 1+rni : floor(Env.width/8)+rni
    for j = 1+rnj : floor(Env.height/7)+rnj
        idx = i + (j-1) * Env.height;
        if ~ismember(idx, Env.aoi)
            Env.grids{idx}(3) = true;
            Env.aoi = [Env.aoi, idx];
            Env.aoi_coord = [Env.aoi_coord; i, j];
        end
    end
end

if length(Env.aoi) ~= length(unique(Env.aoi))
    keyboard
end
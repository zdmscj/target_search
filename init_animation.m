function init_animation(Env, goal_set)

subplot(1,2,1)
set(gcf, 'Position',  [150, 150, 600, 500])
grid on

for i = 1:size(goal_set)
    plot(goal_set(i,1)-0.5, goal_set(i,2)-0.5, 's', 'MarkerEdgeColor',...
        'g', 'MarkerFaceColor','g', 'MarkerSize',20)
    hold on
end

pbaspect([1 1 1])
xlim([0, Env.width])
ylim([0, Env.height])
xticks(1:Env.width)
yticks(1:Env.height)
ax = gca;
ax.LineWidth = 2.5;
ax.GridColor = "#808080";
ax.GridAlpha = 0.8;


subplot(1,2,2)
set(gcf, 'Position',  [650, 650, 1100, 1000])

for i = 1:size(goal_set)
    plot(goal_set(i,1)-0.5, goal_set(i,2)-0.5, 's', 'MarkerEdgeColor',...
        'g', 'MarkerFaceColor','g', 'MarkerSize',20)
    hold on
end

grid on
pbaspect([1 1 1])
xlim([0, Env.width])
ylim([0, Env.height])
xticks(1:Env.width)
yticks(1:Env.height)
ax = gca;
ax.LineWidth = 2.5;
ax.GridColor = "#808080";
ax.GridAlpha = 0.8;


function robot = move_robot(robot, goal)

if norm(goal - robot.q) ~= 0
    q_next = robot.q + robot.t_travel * (goal - robot.q)/norm(goal - robot.q);
    robot.q = q_next;
    robot.traj = [robot.traj;q_next];
end

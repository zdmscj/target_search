function Beta = edge_filtering(Alpha, Beta, Env)

if any(Alpha - 1)
    M = reshape(Alpha-1, [Env.width, Env.height]);
    N = mat2gray(M);
    I = imbinarize(N, 0.05);
    % [~, L] = bwboundaries(I);
    % L = bwlabel(I);
    K = reshape(I, [1, Env.num_grid]);
    Beta(K > 0) = 1;
end
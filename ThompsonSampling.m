function [goal, action, Alpha, Beta, get_reward, no_reward] = ThompsonSampling(Env, ...
    goal, action, action_set, Alpha, Beta, get_reward, no_reward, params)

Reward = zeros(1, length(Env.grids));

range = 1; %ceil(max(Env.width, Env.height)/length(goal_set));
for i = 1 : Env.width
    for j = 1 : Env.height
        idx = i + (j-1) * Env.height;
        if abs(goal(1) - i) <= range && abs(goal(2) - j) <= range         
            Reward(idx) = params.reward;
        else
            Reward(idx) = 0;
        end
    end    
end

if Env.grids{action}(3) 
    Alpha = Alpha + Reward;
    get_reward = get_reward + 1;
    no_reward = 0;
else
    Beta = Beta + Reward;
    no_reward = no_reward + 1;
    get_reward = 0;
end

theta = zeros(1, Env.num_grid);
for k = 1 : Env.num_grid
    if ~isempty(find(ismember(action_set, k), 1))
        theta(k) = -Inf;
    else
        % Should related to maximum likelihood, cost to travel, consistancy
        % in getting or not getting a reward
        if get_reward > 0
            theta(k) = betarnd(Alpha(k),Beta(k)) + (1 - norm(Env.grids{k}(1:2)... 
                 - goal)/sqrt(Env.width^2 + Env.height^2)) * get_reward;
        else
            theta(k) = betarnd(Alpha(k),Beta(k)) + (1 - norm(Env.grids{k}(1:2)... 
                 - goal)/sqrt(Env.width^2 + Env.height^2)) / no_reward;
        end
    end
end
[~, idx] = max(theta);
if idx > Env.num_grid
    keyboard
end
action = idx;
goal = Env.grids{action}(1:2);    


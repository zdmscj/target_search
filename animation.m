function animation(Env, robot, goal_set, varargin)

if nargin == 3
    ts = false;
else
    ts = true;
    Alpha = varargin{1};
    Beta = varargin{2};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ts
    set(gcf, 'Units', 'normalized', 'Position',  [0.1, 0.1, 0.6, 0.8])
    subplot('Position', [0.1, 0.55, 0.4, 0.4])
else
    % subplot(2,2,1)
    set(gcf, 'Units', 'normalized', 'Position',  [0.3, 0.3, 0.4, 0.4])
end
aoi = zeros(length(Env.aoi), 2);
for i = 1 : length(Env.aoi)
    aoi(i,:) = Env.grids{Env.aoi(i)}(1:2) - 0.5;
end
scatter(aoi(:,1), aoi(:,2), 200, 'b', 'filled', 'Marker', 's');
hold on
scatter(goal_set(:,1)' - 0.5, goal_set(:,2)' - 0.5, 200, 'g', 'filled', 'Marker', 's');
hold on
plot(robot.q(1) - 0.5, robot.q(2) - 0.5, 'bo', 'MarkerFaceColor','r', 'MarkerSize',12)
%plot(robot.traj(:,1)' - 0.5, robot.traj(:,2)' - 0.5, 'ko-', 'LineWidth', 1, 'MarkerFaceColor','k')
%hold on
grid on
pbaspect([1 1 1])
xlim([0, Env.width])
ylim([0, Env.height])
xticks(1 : Env.width)
yticks(1 : Env.height)
ax = gca;
ax.LineWidth = 2.5;
ax.GridColor = "#808080";
ax.GridAlpha = 0.8;
title("Ground truth")
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ts
    subplot(2,2,2)
    subplot('Position', [0.5, 0.55, 0.4, 0.4])
    
    x = zeros(1, Env.num_grid);
    y = zeros(1, Env.num_grid);
    c = zeros(1, Env.num_grid);
    if any(Alpha - 1)
        scale_a = max(Alpha);
    else
        scale_a = 30;
    end
    for i = 1 : Env.width
        for j = 1 : Env.height
            idx = i + (j-1) * Env.height;
            c(idx) = 1 - Alpha(idx)/scale_a;
            x(idx) = i - 0.5;
            y(idx) = j - 0.5;
        end
    end
    scatter(x, y, 200, [c',c',c'], 'filled', 'Marker', 's');
    
    grid on
    pbaspect([1 1 1])
    xlim([0, Env.width])
    ylim([0, Env.height])
    xticks(1 : Env.width)
    yticks(1 : Env.height)
    ax = gca;
    ax.LineWidth = 2.5;
    ax.GridColor = "#808080";
    ax.GridAlpha = 0.8;
    title("Alpha")
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    subplot(2,2,3)
    subplot('Position', [0.1, 0.1, 0.4, 0.4])
    
    x = zeros(1, Env.num_grid);
    y = zeros(1, Env.num_grid);
    c = zeros(1, Env.num_grid);
    if any(Beta - 1)
        scale_b = max(Beta);
    else
        scale_b = 30;
    end
    for i = 1 : Env.width
        for j = 1 : Env.height
            idx = i + (j-1) * Env.height;
            c(idx) = 1 - Beta(idx)/scale_b;
            x(idx) = i - 0.5;
            y(idx) = j - 0.5;
        end
    end
    scatter(x, y, 200, [c',c',c'], 'filled', 'Marker', 's');
    
    grid on
    pbaspect([1 1 1])
    xlim([0, Env.width])
    ylim([0, Env.height])
    xticks(1 : Env.width)
    yticks(1 : Env.height)
    ax = gca;
    ax.LineWidth = 2.5;
    ax.GridColor = "#808080";
    ax.GridAlpha = 0.8;
    title("Beta")
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
shg


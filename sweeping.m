function goal = sweeping(Env, goal)

if mod(goal(2), 2) == 1
    temp = [goal(1) + 1, goal(2)];
else
    temp = [goal(1) - 1, goal(2)];
end

if temp(1) > Env.width || temp(1) <= 0
    temp = [goal(1), goal(2) + 1];
end

goal = temp;
